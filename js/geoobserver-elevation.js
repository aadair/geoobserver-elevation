require(["esri/IdentityManager", "esri/config", "esri/map",
    "esri/layers/ArcGISDynamicMapServiceLayer", "esri/dijit/BasemapGallery",
    "esri/arcgis/utils", "esri/tasks/Geoprocessor",
    "elevationtool-gp-js/elevationtool", "coverage-areas-js/coverage-areas",
    "elevation-navigation-js/Navigation", "dojo/on", "dojo/ready",
    "dijit/layout/ContentPane", "dijit/layout/BorderContainer",
    "dijit/TitlePane", "dojo/domReady!"
  ],
  function(IdentityManager, esriConfig, Map, ArcGISDynamicMapServiceLayer,
    BasemapGallery, arcgisUtils, Geoprocessor, ElevationTool,
    CoverageArea, Navigation, on, ready) {

    ready(function() {
      // Proxy page
      esriConfig.defaults.io.proxyUrl = "http://gistest2.ftw.nrcs.usda.gov/DotNet/proxy.ashx";
      esriConfig.defaults.io.alwaysUseProxy = false;

      var myMap = new Map("map", {
        basemap: "topo",
        center: [-92, 38], // long, lat
        zoom: 5,
        slider: false
      });

      var nav = new Navigation({
        map: myMap
      }, "navtools");
      nav.startup();

      // HUC 12 layer
      var huc12_layer = new ArcGISDynamicMapServiceLayer("http://gisdev1.ftw.nrcs.usda.gov/arcgis/rest/services/hydrography/WBD/MapServer", {});
      huc12_layer.setVisibleLayers([3]);
      myMap.addLayer(huc12_layer);

      // CLU layer
      var clu_layer = new ArcGISDynamicMapServiceLayer("http://gisdev1.ftw.nrcs.usda.gov/arcgis/rest/services/lidardata/clu_Scale/MapServer", {});
      myMap.addLayer(clu_layer);

      // NED 30 meter layer
      var ned30m_layer = new ArcGISDynamicMapServiceLayer("http://p119dodgep402.ftw.nrcs.usda.gov/arcgis/rest/services/NED/NED_Metadata/MapServer", {
        "opacity": 0.5
      });
      ned30m_layer.setVisibleLayers([2]);
      myMap.addLayer(ned30m_layer);

      // Coverage Area dijit for NED 30 meter layer
      var coverage_ned30m = new CoverageArea({
        layer: ned30m_layer,
        displayName: "NED 30m",
        checked: false,
        imgData: "iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAAAXNSR0IB2cksfwAAAAlwSFlzAAAOxAAADsQBlSsOGwAAADFJREFUOI1jYaAyYKGZgf//7/tPiUGMjE6MKAZSC4waOGrgqIGjBtLZQFh5RjUDqQUAxuoElsWJpuwAAAAASUVORK5CYII="
      }, "coverage_ned30m");

      // NED 10 meter layer
      var ned10m_layer = new ArcGISDynamicMapServiceLayer("http://p119dodgep402.ftw.nrcs.usda.gov/arcgis/rest/services/NED/NED_Metadata/MapServer", {
        "opacity": 0.5
      });
      ned10m_layer.setVisibleLayers([1]);
      myMap.addLayer(ned10m_layer);

      // Coverage Area dijit for NED 10 meter layer
      var coverage_ned10m = new CoverageArea({
        layer: ned10m_layer,
        displayName: "NED 10m",
        checked: false,
        imgData: "iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAAAXNSR0IB2cksfwAAAAlwSFlzAAAOxAAADsQBlSsOGwAAAC9JREFUOI1jYaAyYKGdgdHP/lNk0lIpRlQDqQRGDRw1cNTAUQPpbSC0PKOegVQCAEgOBBHrbs3dAAAAAElFTkSuQmCC"
      }, "coverage_ned10m");

      // IFSAR 5 meter layer
      var ifsar5m_layer = new ArcGISDynamicMapServiceLayer("http://p119dodgep402.ftw.nrcs.usda.gov/arcgis/rest/services/IFSAR/IFSARboundaries/MapServer", {
        "opacity": 0.5
      });
      myMap.addLayer(ifsar5m_layer);

      // Coverage Area dijit for IFSAR 5 meter layer
      var coverage_ifsar5m = new CoverageArea({
        layer: ifsar5m_layer,
        displayName: "IFSAR 5m",
        checked: true,
        imgData: "iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAAAXNSR0IB2cksfwAAAAlwSFlzAAAOxAAADsQBlSsOGwAAAENJREFUOI1jYaAyYGFgYGDoyHf9Tw3DKibuZmShhkHIAMXAiom7GckxBNmHtHXhqIGjBo4aODwMpEa5SBsXklsOYgMA1AsMUa4B3s4AAAAASUVORK5CYII="
      }, "coverage_ifsar5m");

      // NED 3 meter layer
      var ned3m_layer = new ArcGISDynamicMapServiceLayer("http://p119dodgep402.ftw.nrcs.usda.gov/arcgis/rest/services/NED/NED_Metadata/MapServer", {
        "opacity": 0.5
      });
      ned3m_layer.setVisibleLayers([0]);
      myMap.addLayer(ned3m_layer);

      // Coverage Area dijit for NED 3 meter layer
      var coverage_ned3m = new CoverageArea({
        layer: ned3m_layer,
        displayName: "NED 3m",
        checked: true,
        imgData: "iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAAAXNSR0IB2cksfwAAAAlwSFlzAAAOxAAADsQBlSsOGwAAADFJREFUOI1jYaAyYKGZgdPvfPpPiUGZKnyMKAZSC4waOGrgqIGjBtLZQFh5RjUDqQUA5W4E7T2J3aUAAAAASUVORK5CYII="
      }, "coverage_ned3m");

      // LiDAR 2 meter layer
      var lidar2m_layer = new ArcGISDynamicMapServiceLayer("http://p119dodgep402.ftw.nrcs.usda.gov/arcgis/rest/services/LiDAR/Bare_Earth_1M2M_Metadata_Boundaries/MapServer", {
        "opacity": 0.5
      });
      lidar2m_layer.setVisibleLayers([1]);
      myMap.addLayer(lidar2m_layer);

      // Coverage Area dijit for LiDAR 2 meter layer
      var coverage_lidar2m = new CoverageArea({
        layer: lidar2m_layer,
        displayName: "LiDAR 2m",
        checked: true,
        imgData: "iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAAAXNSR0IB2cksfwAAAAlwSFlzAAAOxAAADsQBlSsOGwAAAENJREFUOI1jYaAyYGFgYGD4v5LhPzUMYwxnYGShhkHIAMVAxnAGRnIMQfYhbV04auCogaMGDg8DqVEu0saF5JaD2AAAyx4JUUyXUxgAAAAASUVORK5CYII="
      }, "coverage_lidar2m");

      // LiDAR 1 meter layer
      var lidar1m_layer = new ArcGISDynamicMapServiceLayer("http://p119dodgep402.ftw.nrcs.usda.gov/arcgis/rest/services/LiDAR/Bare_Earth_1M2M_Metadata_Boundaries/MapServer", {
        "opacity": 0.5
      });
      lidar1m_layer.setVisibleLayers([0]);
      myMap.addLayer(lidar1m_layer);

      // Coverage Area dijit for LiDAR 1 meter layer
      var coverage_lidar1m = new CoverageArea({
        layer: lidar1m_layer,
        displayName: "LiDAR 1m",
        checked: true,
        imgData: "iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAAAXNSR0IB2cksfwAAAAlwSFlzAAAOxAAADsQBlSsOGwAAAEFJREFUOI1jYaAyYGFgYGBgaF3xnyqmVUcwslDFICSAamB1BCNZpiD5kMYuHDVw1MBRA4eHgVQoF2nkQnLLQSwAAHNwCVG6ibfmAAAAAElFTkSuQmCC"
      }, "coverage_lidar1m");

      // Geoprocessor for AOI info (synchronous)
      var gp1 = new Geoprocessor("http://gistest2.ftw.nrcs.usda.gov:6080/arcgis/rest/services/elevation/AOIinfo/GPServer/AOI%20Information");

      // Geoprocessor for AOI polygon (synchronous)
      var gp2 = new Geoprocessor("http://gistest2.ftw.nrcs.usda.gov:6080/arcgis/rest/services/elevation/AOIpolygon/GPServer/AOI%20Polygon");

      // Geoprocessor for Data extract (asynchronous)
      var gp3 = new Geoprocessor("http://gistest2.ftw.nrcs.usda.gov:6080/arcgis/rest/services/elevation/DataExtract/GPServer/Data%20extract");

      var myWidget = new ElevationTool({
        map: myMap,
        gpAOIinfo: gp1,
        gpAOIpolygon: gp2,
        gpDataExtract: gp3,
      }, "elevationtool_draw");
      myWidget.startup();

      var basemapgallery = new BasemapGallery({
        showArcGISBasemaps: true,
        map: myMap
      }, "basemapgallery");
      basemapgallery.startup();

    });
  });
